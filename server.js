require('dotenv').load();

var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var app = express();
var port = process.env.PORT || 4000;

require('./server/models/db');
require('./server/lib/auth.js');

if (process.env.NODE_ENV === 'development') {
  var seedAdmin = require('./server/models/seeds/admin');
  var seedProducts = require('./server/models/seeds/products');
  seedAdmin();
  seedProducts();
}

var adminRoutes = require('./server/routes/admin');
var authRoutes = require('./server/routes/auth');
var braintreeRoutes = require('./server/routes/braintree');
var apiRoutes = require('./server/routes/api');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'client')));

app.use('/admin', adminRoutes);
app.use('/auth', authRoutes);
app.use('/braintree', braintreeRoutes);
app.use('/api', apiRoutes);

app.use(function(req, res) {
  res.sendFile(path.join(__dirname, 'client', 'index.html'));
});

app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

app.use(function (err, req, res, next) {
  if (err.name === 'UnauthorizedError') {
    res.status(401);
    res.json({"message" : err.name + ": " + err.message});
  }
});

if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

app.listen(port);
console.log('Listening on port ' + port);

module.exports = app;