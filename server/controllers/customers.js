var mongoose = require('mongoose');
var Customer = mongoose.model('Customer');

module.exports.getCustomerById = function(req, res, next, customerId) {
  Customer
    .findById(customerId)
    .exec(function(err, customer) {
      if (err) {
        res.status(404).json(err);
      } else if (!customer) {
        res.status(404).json({
          message: 'customerId not found.'
        });
      } else {
        req.customer = customer;
        next();
      }
    });
};

module.exports.list = function(req, res) {
  Customer.find(function(err, customers) {
    if (err) {
      res.status(404).json(err);
    } else {
      res.status(200).json(customers);
    }
  });
};

module.exports.create = function(req, res) {
  if (!req.body.email && !req.body.password) {
    return res.status(400).json({
      error: 'Email and password are required.'
    });
  }

  if (req.body.password.length < 6 || req.body.password.length > 20) {
    return res.status(400).json({
      error: 'Password must be 6-20 characters.'
    });
  }

  var customer = new Customer(req.body);

  customer.save(function(err, newCustomer) {
    if (err) {
      res.status(404).json(err);
    } else {
      res.status(200).json(newCustomer);
    }
  });
};

module.exports.readOne = function(req, res) {
  res.json(req.customer);
};

module.exports.update = function(req, res) {
  if (req.body.password.length < 6 || req.body.password.length > 20) {
    return res.status(400).json({
      error: 'Password must be 6-20 characters.'
    });
  }

  var customer = req.customer;
  customer.email = req.body.email;
  customer.password = req.body.password;

  customer.save(function(err, newCustomer) {
    if (err) {
      res.status(404).json(err);
    } else {
      res.status(200).json(newCustomer);
    }
  });
};

module.exports.delete = function(req, res) {
  var customer = req.customer;

  customer.remove(function(err) {
    if (err) {
      res.status(404).json(err);
    } else {
      res.status(204).json(null);
    }
  });
};