var mongoose = require('mongoose');
var Product = mongoose.model('Product');

module.exports.getProductById = function(req, res, next, productId) {
  Product
    .findById(productId)
    .exec(function(err, product) {
      if (err) {
        res.status(404).json(err);
      } else if (!product) {
        res.status(404).json({
          message: 'productId not found.'
        });
      } else {
        req.product = product;
        next();
      }
    });
};

module.exports.list = function(req, res) {
  Product.find(function(err, products) {
    if (err) {
      res.status(404).json(err);
    } else {
      res.status(200).json(products);
    }
  });
};

module.exports.create = function(req, res) {
  if (!req.body.name) {
    res.status(400).json({
      error: 'Name is required.'
    });
    return;
  }

  var product = new Product(req.body);

  product.save(function(err, newProduct) {
    if (err) {
      res.status(404).json(err);
    } else {
      res.status(200).json(newProduct);
    }
  });
};

module.exports.readOne = function(req, res) {
  res.json(req.product);
};

module.exports.update = function(req, res) {
  var product = req.product;
  product.name = req.body.name;
  product.description = req.body.description;
  product.image = req.body.image;
  product.price = req.body.price;
  product.quantity = req.body.quantity;

  product.save(function(err, newProduct) {
    if (err) {
      res.status(404).json(err);
    } else {
      res.status(200).json(newProduct);
    }
  });
};

module.exports.delete = function(req, res) {
  var product = req.product;

  product.remove(function(err) {
    if (err) {
      res.status(404).json(err);
    } else {
      res.status(204).json(null);
    }
  });
};