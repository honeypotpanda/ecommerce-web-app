var mongoose = require('mongoose');
var Admin = mongoose.model('Admin');
var passport = require('passport');

module.exports.login = function(req, res) {
  if (!req.body.email || !req.body.password) {
    return res.status(400).json({
      message: 'All fields are required.'
    });
  }

  passport.authenticate('admin-local', function(err, admin, info) {
    if (err) {
      return res.status(404).json(err);
    }

    if (admin) {
      res.status(200).json({
        token: admin.generateJwt()
      });
    } else {
      res.status(401).json(info);
    }
  })(req, res);
};