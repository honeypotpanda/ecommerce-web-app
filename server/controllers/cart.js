var mongoose = require('mongoose');
var Customer = mongoose.model('Customer');

module.exports.get = function(req, res) {
  Customer
    .findById(req.customer._id)
    .select('cart -_id')
    .populate('cart.productId')
    .exec(function(err, cart) {
      res.status(200).json(cart);
    });
};

module.exports.addProduct = function(req, res) {
  var customer = req.customer;
  var product = req.product;

  // if (req.body.quantity > product.quantity) {
  //   var errorMessage = "There isn't enough " + product.name + " available.";
  //   return res.status(400).json({
  //     error: errorMessage
  //   });
  // }

  customer.cart.push({
    productId: product._id,
    quantity: req.body.quantity
  });

  customer.save(function(err, newCustomer) {
    if (err) {
      res.status(404).json(err);
    } else {
      res.status(200).json({
        message: 'Successfully added ' + product.name + ' to cart.'
      });
    }
  });
};

module.exports.updateProduct = function(req, res) {
  var customer = req.customer;
  var product = req.product;

  // if (req.body.quantity > product.quantity) {
  //   var errorMessage = "There isn't enough " + product.name + " available.";
  //   return res.status(400).json({
  //     error: errorMessage
  //   });
  // }

  var cart = customer.cart;

  for (var i = 0; i < cart.length; i++) {
    var item = cart[i];
    if (item.productId.equals(product._id)) {
      item.quantity = req.body.quantity;
      break;
    }
  }

  customer.save(function(err, newCustomer) {
    if (err) {
      res.status(404).json(err);
    } else {
      res.status(200).json({
        message: 'Successfully updated ' + product.name + ' in cart.'
      });
    }
  });
};

module.exports.removeItem = function(req, res) {
  var customer = req.customer;
  var product = req.product;
  var cart = customer.cart;

  for (var i = 0; i < cart.length; i++) {
    if (cart[i].productId.equals(product._id)) {
      cart.splice(i, 1);
      break;
    }
  }

  customer.save(function(err, newCustomer) {
    if (err) {
      res.status(404).json(err);
    } else {
      res.status(200).json({
        message: 'Successfully deleted ' + product.name + ' in cart.'
      });
    }
  });
};