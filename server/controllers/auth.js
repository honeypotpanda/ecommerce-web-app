var mongoose = require('mongoose');
var Customer = mongoose.model('Customer');
var passport = require('passport');

module.exports.register = function(req, res) {
  if(!req.body.email || !req.body.password) {
    return res.status(400).json({
      message: 'All fields are required.'
    });
  }

  var customer = new Customer();

  customer.email = req.body.email;
  customer.password = req.body.password;

  customer.save(function(err, customer) {
    if (err) {
      res.status(400).json(err);
    } else {
      res.status(200).json({
        token: customer.generateJwt()
      });
    }
  });
};

module.exports.login = function(req, res) {
  if (!req.body.email || !req.body.password) {
    return res.status(400).json({
      message: 'All fields are required.'
    });
  }

  passport.authenticate('customer-local', function(err, customer, info) {
    if (err) {
      return res.status(404).json(err);
    }

    if (customer) {
      res.status(200).json({
        token: customer.generateJwt()
      });
    } else {
      res.status(401).json(info);
    }
  })(req, res);
};