var mongoose = require('mongoose');
var Customer = mongoose.model('Customer');

module.exports.getOrders = function(req, res) {
  var customer = req.customer;

  Customer
    .findById(customer._id)
    .populate('orders.order.productId')
    .exec(function(err, customer) {
      if (err) {
        return res.status(404).json(err);
      }
      res.status(200).json(customer.orders);
    });
};

module.exports.addOrder = function(req, res) {
  var customer = req.customer;

  Customer
    .findById(customer._id)
    .populate('cart.productId')
    .exec(function(err, newCustomer) {
      var cart = newCustomer.cart;

      for (var i = 0; i < cart.length; i++) {
        var productName = cart[i].productId.name;
        var productQuantity = cart[i].productId.quantity;

        if (cart[i].quantity > productQuantity) {
          var errorMessage = 'There are only ' + productQuantity + ' of ' + productName + ' left.';
          return res.status(400).json({
            error: errorMessage
          });
        }
      }

      customer.orders.push({ order: customer.cart });
      customer.cart = [];

      customer.save(function(err, newCustomer) {
        if (err) {
          res.status(404).json(err);
        } else {
          res.status(200).json(newCustomer);
        }
      });
    });
};