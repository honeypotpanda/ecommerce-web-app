var braintree = require('braintree');
var merchantId = process.env.MERCHANT_ID;
var publicKey = process.env.PUBLIC_KEY;
var privateKey = process.env.PRIVATE_KEY;
var gateway = braintree.connect({
  environment: braintree.Environment.Sandbox,
  merchantId: merchantId,
  publicKey: publicKey,
  privateKey: privateKey
});

module.exports.generateClientToken = function(req, res) {
  gateway.clientToken.generate({}, function(err, response) {
    if (err) {
      res.status(404).json(err);
    } else {
      res.status(200).json(response.clientToken);
    }
  });
};

module.exports.checkout = function(req, res) {
  var nonce = req.body.paymentMethodNonce;
  var amount = req.body.amount || 10;
  gateway.transaction.sale({
    amount: amount,
    paymentMethodNonce: nonce,
    options: {
      submitForSettlement: true
    }
  }, function (err, result) {
    if (err) {
      return res.status(400).json(err);
    }

    if (result.success) {
      res.status(200).json({
        message: 'Success!'
      });
    } else {
      res.status(400).json({
        message: 'Something went wrong...'
      });
    }
  });
};