var express = require('express');
var router = express.Router();

var braintreeCtrl = require('../controllers/braintree');

router.get('/token', braintreeCtrl.generateClientToken);
router.post('/checkout', braintreeCtrl.checkout);

module.exports = router;