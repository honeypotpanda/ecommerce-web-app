var express = require('express');
var router = express.Router();

var authCtrl = require('../controllers/auth');
var helpers = require('../lib/helpers');

router.post('/register', authCtrl.register);
router.post('/login', authCtrl.login);
router.post('/token', helpers.validateToken);

module.exports = router;