var express = require('express');
var router = express.Router();

var adminCtrl = require('../controllers/admin');

router.post('/login', adminCtrl.login);

module.exports = router;