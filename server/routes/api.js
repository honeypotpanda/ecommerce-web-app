var express = require('express');
var router = express.Router();
var helpers = require('../lib/helpers');

var productsCtrl = require('../controllers/products');
var customersCtrl = require('../controllers/customers');
var cartCtrl = require('../controllers/cart');
var ordersCtrl = require('../controllers/orders');

router.param('productId', productsCtrl.getProductById);
router.get('/products', productsCtrl.list);
router.post('/products', helpers.validateToken, helpers.ensureAdmin, productsCtrl.create);
router.get('/products/:productId', productsCtrl.readOne);
router.put('/products/:productId', helpers.validateToken, helpers.ensureAdmin, productsCtrl.update);
router.delete('/products/:productId', helpers.validateToken, helpers.ensureAdmin, productsCtrl.delete);

router.param('customerId', customersCtrl.getCustomerById);
router.get('/customers', helpers.validateToken, helpers.ensureAdmin, customersCtrl.list);
router.post('/customers', helpers.validateToken, helpers.ensureAdmin, customersCtrl.create);
router.get('/customers/:customerId', customersCtrl.readOne);
router.put('/customers/:customerId', helpers.validateToken, helpers.ensureAuthorizedCustomer, customersCtrl.update);
router.delete('/customers/:customerId', helpers.validateToken, helpers.ensureAuthorizedCustomer, customersCtrl.delete);

router.get('/customers/:customerId/cart', helpers.validateToken, helpers.ensureAuthorizedCustomer, cartCtrl.get);
router.post('/customers/:customerId/cart/:productId', helpers.validateToken, helpers.ensureAuthorizedCustomer, cartCtrl.addProduct);
router.put('/customers/:customerId/cart/:productId', helpers.validateToken, helpers.ensureAuthorizedCustomer, cartCtrl.updateProduct);
router.delete('/customers/:customerId/cart/:productId', helpers.validateToken, helpers.ensureAuthorizedCustomer, cartCtrl.removeItem);

router.get('/customers/:customerId/orders', helpers.validateToken, helpers.ensureAuthorizedCustomer, ordersCtrl.getOrders);
router.post('/customers/:customerId/orders', helpers.validateToken, helpers.ensureAuthorizedCustomer, ordersCtrl.addOrder);

module.exports = router;