var jwt = require('express-jwt');

var validateToken = jwt({
  secret: process.env.JWT_SECRET,
  userProperty: 'payload'
});

var ensureAdmin = function(req, res, next) {
  if (req.payload.role === 'admin') {
    return next();
  }

  res.status(401).json({
    message: 'You do not have permission to do that.'
  });
};

var ensureAuthorizedCustomer = function(req, res, next) {
  if (req.payload._id == req.customer._id) {
    return next();
  }

  res.status(401).json({
    message: 'You do not have permission to do that.'
  });
};

module.exports = {
  validateToken: validateToken,
  ensureAdmin: ensureAdmin,
  ensureAuthorizedCustomer: ensureAuthorizedCustomer
};