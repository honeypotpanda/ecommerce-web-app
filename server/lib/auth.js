var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var mongoose = require('mongoose');
var Customer = mongoose.model('Customer');
var Admin = mongoose.model('Admin');

passport.use('customer-local', new LocalStrategy({
    usernameField: 'email',
    passReqToCallback: true
  },
  function(req, email, password, done) {
    Customer.findOne({ email: email }, function(err, customer) {
      if (err) {
        return done(err);
      }

      if (!customer) {
        return done(null, false, {
          message: 'Your email or password was incorrect. Please try again.'
        });
      }

      customer.comparePassword(password, function(err, isMatch) {
        if (err) {
          return done(err);
        }

        if (isMatch) {
          return done(null, customer);
        } else {
          return done(null, false, {
            message: 'Your email or password was incorrect. Please try again.'
          });
        }
      });
    });
  }
));

passport.use('admin-local', new LocalStrategy({
    usernameField: 'email',
    passReqToCallback: true
  },
  function(req, email, password, done) {
    Admin.findOne({ email: email }, function(err, admin) {
      if (err) {
        return done(err);
      }

      if (!admin) {
        return done(null, false, {
          message: 'Your email or password was incorrect. Please try again.'
        });
      }

      admin.comparePassword(password, function(err, isMatch) {
        if (err) {
          return done(err);
        }

        if (isMatch) {
          return done(null, admin);
        } else {
          return done(null, false, {
            message: 'Your email or password was incorrect. Please try again.'
          });
        }
      });
    });
  }
));