var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');
var SALT_WORK_FACTOR;

if (process.env.NODE_ENV === 'test') {
  SALT_WORK_FACTOR = 1;
} else {
  SALT_WORK_FACTOR = 10;
}

var adminSchema = Schema({
  email: {
    type: String,
    required: true,
    unique: true,
    lowercase: true
  },
  password: {
    type: String,
    required: true
  }
});

adminSchema.methods.comparePassword = function(password, done) {
  bcrypt.compare(password, this.password, function(err, isMatch) {
    if (err) {
      return done(err);
    }

    return done(null, isMatch);
  });
};

adminSchema.methods.generateJwt = function() {
  var expiry = new Date();
  expiry.setDate(expiry.getDate() + 7);

  return jwt.sign({
    _id: this._id,
    role: 'admin',
    exp: parseInt(expiry.getTime() / 1000)
  }, process.env.JWT_SECRET);
};

adminSchema.pre('save', function(next) {
  var admin = this;

  if (!admin.isModified('password')) {
    return next();
  }

  bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
    if (err) {
      return next(err);
    }

    bcrypt.hash(admin.password, salt, function(err, hash) {
      if (err) {
        return next(err);
      }

      admin.password = hash;
      next();
    });
  });
});

mongoose.model('Admin', adminSchema);