var mongoose = require('mongoose');
var Product = mongoose.model('Product');

var seedProduct = function() {
  Product.find(function(err, products) {
    if (err) {
      throw err;
    }

    if (products.length === 0) {
      for (var i = 1; i < 6; i++) {
        var product = new Product({
          name: 'product' + i,
          description: 'description' + i,
          image: 'https://static.pexels.com/photos/28742/pexels-photo-28742.jpg',
          price: i,
          quantity: i
        });

        product.save();
      }

      console.log('Dummy products have been created!');
    }
  });
};

module.exports = seedProduct;