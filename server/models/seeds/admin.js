var mongoose = require('mongoose');
var Admin = mongoose.model('Admin');

var seedAdmin = function() {
  Admin.find(function(err, admins) {
    if (err) {
      throw err;
    }

    if (admins.length === 0) {
      var admin = new Admin({
        email: 'admin@admin.com',
        password: 'admin'
      });

      admin.save(function(err, admin) {
        if (err) {
          throw err;
        }

        console.log('Admin has been created!');
      });
    }
  });
};

module.exports = seedAdmin;