var mongoose = require('mongoose');
var gracefulShutdown;
var dbUri = 'mongodb://localhost/ecommerce-web-app';

if (process.env.NODE_ENV === 'test') {
  dbUri = 'mongodb://localhost/ecommerce-web-app-test';
} else if (process.env.NODE_ENV === 'production') {
  dbUri = process.env.MONGOLAB_URI;
}

mongoose.connect(dbUri);

mongoose.connection.on('connected', function() {
  console.log('Mongoose connected to ' + dbUri);
});

mongoose.connection.on('error', function(err) {
  console.log('Mongoose connection error: ' + err);
});

mongoose.connection.on('disconnected', function() {
  console.log('Mongoose disconnected');
});

gracefulShutdown = function(msg, callback) {
  mongoose.connection.close(function() {
    console.log('Mongoose disconnected through ' + msg);
    callback();
  });
};

process.once('SIGUSR2', function() {
  gracefulShutdown('nodemon restart', function() {
    process.kill(process.pid, 'SIGUSR2');
  });
});

process.on('SIGINT', function() {
  gracefulShutdown('server termination', function() {
    process.exit(0);
  });
});

process.on('SIGTERM', function() {
  gracefulShutdown('Heroku server termination', function() {
    process.exit(0);
  });
});

require('./product');
require('./customer');
require('./admin');