var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');
var SALT_WORK_FACTOR;

if (process.env.NODE_ENV === 'test') {
  SALT_WORK_FACTOR = 1;
} else {
  SALT_WORK_FACTOR = 10;
}

var productSchema = Schema({
  productId: {
    type: Schema.Types.ObjectId,
    ref: 'Product'
  },
  quantity: {
    type: Number,
    default: 1
  }
});

var orderSchema = Schema({
  order: [productSchema],
  date: {
    type: Date,
    default: Date.now
  }
});

var customerSchema = Schema({
  email: {
    type: String,
    required: true,
    unique: true,
    lowercase: true
  },
  password: {
    type: String,
    required: true
  },
  cart: [productSchema],
  orders: [orderSchema]
});

customerSchema.methods.comparePassword = function(password, done) {
  bcrypt.compare(password, this.password, function(err, isMatch) {
    if (err) {
      return done(err);
    }

    return done(null, isMatch);
  });
};

customerSchema.methods.generateJwt = function() {
  var expiry = new Date();
  expiry.setDate(expiry.getDate() + 7);

  return jwt.sign({
    _id: this._id,
    role: 'customer',
    exp: parseInt(expiry.getTime() / 1000)
  }, process.env.JWT_SECRET);
};

customerSchema.pre('save', function(next) {
  var customer = this;

  if (!customer.isModified('password')) {
    return next();
  }

  bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
    if (err) {
      return next(err);
    }

    bcrypt.hash(customer.password, salt, function(err, hash) {
      if (err) {
        return next(err);
      }

      customer.password = hash;
      next();
    });
  });
});

mongoose.model('Customer', customerSchema);