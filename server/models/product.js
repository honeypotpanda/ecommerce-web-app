var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var productSchema = Schema({
  name: {
    type: String,
    required: true
  },
  description: String,
  image: String,
  price: Number,
  quantity: {
    type: Number,
    default: 0
  }
});

mongoose.model('Product', productSchema);