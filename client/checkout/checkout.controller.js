(function() {
  angular
    .module('EcommerceApp')
    .controller('CheckoutCtrl', CheckoutCtrl);

  CheckoutCtrl.$inject = ['Authentication', 'Braintree', 'Toast', 'Cart', 'Orders'];

  function CheckoutCtrl(Authentication, Braintree, Toast, Cart, Orders) {
    var vm = this;

    vm.currentUser = Authentication.currentUser();
    vm.grandTotal = 0;

    Cart.get(vm.currentUser._id)
      .success(function(res) {
        vm.cart = res.cart;
        vm.grandTotal = vm.cart.reduce(function(acc, item) {
          return acc + item.quantity * item.productId.price;
        }, 0);
      })
      .error(function(res) {
        vm.error = 'There was an error retrieving your shopping cart.';
      });

    Braintree.getNonce()
      .success(function(nonce) {
        braintree.setup(nonce, "dropin", {
          container: "payment-form",
          onPaymentMethodReceived: function(obj) {
            var body = {
              paymentMethodNonce: obj.nonce,
              amount: vm.grandTotal
            };

            Braintree.checkout(body)
              .success(function(res) {
                Orders.addOrder(vm.currentUser._id)
                  .success(function(res) {
                    Toast.showSimpleToast('Successfully placed order.');
                  })
                  .error(function(res) {
                    Toast.showSimpleToast(res);
                  });
              })
              .error(function(res) {
                Toast.showSimpleToast(res.message);
              });
          }
        });
      })
      .error(function(res) {
        Toast.showSimpleToast('Unable to get nonce...');
      });
  }
})();
