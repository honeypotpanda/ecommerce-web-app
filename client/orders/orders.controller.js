(function() {
  angular
    .module('EcommerceApp')
    .controller('OrdersCtrl', OrdersCtrl);

  OrdersCtrl.$inject = ['Authentication', 'Orders', 'Toast'];

  function OrdersCtrl(Authentication, Orders, Toast) {
    var vm = this;
    vm.currentUser = Authentication.currentUser();
    vm.calcTotal = function(order) {
      return order.reduce(function(total, item) {
        return total + item.quantity * item.productId.price;
      }, 0);
    };
    Orders.getOrders(vm.currentUser._id)
      .success(function(res) {
        vm.orders = res;
      })
      .error(function(res) {
        Toast.showSimpleToast(res);
      });
  }
})();
