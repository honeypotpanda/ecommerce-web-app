(function() {
  angular
    .module('EcommerceApp')
    .controller('HomeCtrl', HomeCtrl);

  HomeCtrl.$inject = ['Authentication', 'Products', 'Cart', 'Toast', 'Error'];

  function HomeCtrl(Authentication, Products, Cart, Toast, Error) {
    var vm = this;

    if (Error.invalidToken) {
      Toast.showSimpleToast('Your token was invalid.');
      Error.invalidToken = false;
    }
    
    vm.currentUser = Authentication.currentUser();

    if (!vm.currentUser) {
      return;
    }

    vm.products = [];
    vm.cart = {};

    vm.updateCart = function(productId, quantity) {
      if (!quantity || quantity < 1) {
        console.log(quantity);
        return Toast.showSimpleToast('Quantity must be greater than or equal to 1.');
      }

      if (productId in vm.cart) {
        var newQuantity = quantity + vm.cart[productId];
        Cart.update(vm.currentUser._id, productId, newQuantity)
          .success(function(res) {
            Toast.showSimpleToast(res.message);
            vm.cart[productId] = newQuantity;
          })
          .error(function(res) {
            Toast.showSimpleToast(res);
          });
      } else {
        Cart.add(vm.currentUser._id, productId, quantity)
          .success(function(res) {
            Toast.showSimpleToast(res.message);
            vm.cart[productId] = quantity;
          })
          .error(function(res) {
            Toast.showSimpleToast(res);
          });
      }
    };

    Products.list()
      .success(function(res) {
        vm.products = res;
      })
      .error(function(res) {
        Toast.showSimpleToast(res);
      });

    Cart.get(vm.currentUser._id)
      .then(function(success) {
        success.data.cart.forEach(function(item) {
          vm.cart[item.productId._id] = item.quantity;
        });
      }, function(error) {
        Toast.showSimpleToast(error);
      });
  }
})();
