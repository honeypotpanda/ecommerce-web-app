(function() {
  angular
    .module('EcommerceApp')
    .controller('CartCtrl', CartCtrl);

  CartCtrl.$inject = ['Authentication', 'Cart', 'Toast'];

  function CartCtrl(Authentication, Cart, Toast) {
    var vm = this;

    vm.currentUser = Authentication.currentUser();
    vm.grandTotal = 0;

    Cart.get(vm.currentUser._id)
      .success(function(res) {
        vm.cart = res.cart;
        vm.grandTotal = vm.cart.reduce(function(acc, item) {
          return acc + item.quantity * item.productId.price;
        }, 0);
      })
      .error(function(res) {
        vm.error = 'There was an error retrieving your shopping cart.';
      });

    vm.removeItem = function(index, item) {
      Cart.remove(vm.currentUser._id, item.productId._id)
        .success(function(res) {
          Toast.showSimpleToast('Successfully deleted ' + item.productId.name + '.');
          vm.cart.splice(index, 1);
          vm.grandTotal -= item.quantity * item.productId.price;
        })
        .error(function(res) {
          Toast.showSimpleToast('There was an error deleting ' + item.productId.name + '.');
        });
    };
  }
})();
