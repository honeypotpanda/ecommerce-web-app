(function() {
  angular.module('EcommerceApp', ['ngRoute', 'ngMaterial']);

  var adminRoutes = new Set(['/admin/dashboard']);
  var customerRoutes = new Set(['/', '/cart', '/checkout', '/orders']);
  var unauthRoutes = new Set(['/', '/admin/login', '/login', '/register']);

  function config($routeProvider, $locationProvider, $httpProvider, $mdThemingProvider) {
    $routeProvider
      .when('/', {
        templateUrl: '/home/home.view.html',
        controller: 'HomeCtrl',
        controllerAs: 'vm'
      })
      .when('/admin/login', {
        templateUrl: '/common/login/login.view.html',
        controller: 'LoginCtrl',
        controllerAs: 'vm'
      })
      .when('/admin/dashboard', {
        templateUrl: '/admin/dashboard/dashboard.view.html',
        controller: 'AdminDashboardCtrl',
        controllerAs: 'vm'
      })
      .when('/login', {
        templateUrl: '/common/login/login.view.html',
        controller: 'LoginCtrl',
        controllerAs: 'vm'
      })
      .when('/register', {
        templateUrl: '/register/register.view.html',
        controller: 'RegisterCtrl',
        controllerAs: 'vm'
      })
      .when('/orders', {
        templateUrl: '/orders/orders.view.html',
        controller: 'OrdersCtrl',
        controllerAs: 'vm'
      })
      .when('/cart', {
        templateUrl: '/cart/cart.view.html',
        controller: 'CartCtrl',
        controllerAs: 'vm'
      })
      .when('/checkout', {
        templateUrl: '/checkout/checkout.view.html',
        controller: 'CheckoutCtrl',
        controllerAs: 'vm'
      })
      .otherwise({redirectTo: '/'});

    $locationProvider.html5Mode(true);
    $httpProvider.interceptors.push('AuthInterceptor');
    $mdThemingProvider.theme('default');
  }

  function run($rootScope, $location, adminRoutes, customerRoutes, unauthRoutes, Authentication, Error) {
    $rootScope.$on('$routeChangeStart', function(event, args) {
      var argsRoute = args.$$route;
      if (argsRoute) {
        var path = argsRoute.redirectTo || argsRoute.originalPath;
        var currentUser = Authentication.currentUser();
        var role = currentUser ? currentUser.role : undefined;

        if (!currentUser && !unauthRoutes.has(path)) {
          $location.path('/login');
          $location.replace();
        }

        if (currentUser) {
          Authentication.validateToken()
            .then(function(res) {
              if (role === 'customer' && !customerRoutes.has(path)) {
                $location.path('/');
                $location.replace();
              }

              if (role === 'admin' && !adminRoutes.has(path)) {
                $location.path('/admin/dashboard');
                $location.replace();
              }
            }, function(res) {
              Error.invalidToken = true;
              Authentication.logout();
              $location.path('/');
              $location.replace();
            });
        }
      }
    });
  }

  angular
    .module('EcommerceApp')
    .constant('adminRoutes', adminRoutes)
    .constant('customerRoutes', customerRoutes)
    .constant('unauthRoutes', unauthRoutes)
    .config(['$routeProvider', '$locationProvider', '$httpProvider', '$mdThemingProvider', config])
    .run(['$rootScope', '$location', 'adminRoutes', 'customerRoutes', 'unauthRoutes', 'Authentication', 'Error', run]);
})();