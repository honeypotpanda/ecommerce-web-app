(function() {
  angular
    .module('EcommerceApp')
    .controller('AddDialogCtrl', AddDialogCtrl);

  AddDialogCtrl.$inject = ['$mdDialog', 'Products'];

  function AddDialogCtrl($mdDialog, Products) {
    var vm = this;

    vm.newProduct = {};

    vm.cancel = function() {
      $mdDialog.cancel();
    };

    vm.add = function() {
      vm.error = '';

      if (!vm.newProduct.name || !vm.newProduct.description || !vm.newProduct.image ||
        !vm.newProduct.price || !vm.newProduct.quantity) {
        vm.error = 'All fields are required.';
        return;
      }

      Products.create(vm.newProduct)
        .success(function(res) {
          $mdDialog.hide(res);
        })
        .error(function(res) {
          vm.error = res;
        });
    };
  }
})();