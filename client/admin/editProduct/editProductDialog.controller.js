(function() {
  angular
    .module('EcommerceApp')
    .controller('EditDialogCtrl', EditDialogCtrl);

  EditDialogCtrl.$inject = ['$mdDialog', 'Products', 'product'];

  function EditDialogCtrl($mdDialog, Products, product) {
    var vm = this;

    vm.productName = product.name;

    vm.updatedProduct = {
      _id: product._id,
      name: product.name,
      description: product.description,
      image: product.image,
      price: product.price,
      quantity: product.quantity
    };

    vm.cancel = function() {
      $mdDialog.cancel();
    };

    vm.confirm = function() {
      vm.error = '';

      if (!vm.updatedProduct.name || !vm.updatedProduct.description || !vm.updatedProduct.image ||
          !vm.updatedProduct.price || !vm.updatedProduct.quantity) {
        vm.error = 'All fields are required.';
        return;
      }

      Products.updateProduct(vm.updatedProduct)
        .success(function(res) {
          $mdDialog.hide(res);
        })
        .error(function(res) {
          vm.error = res;
        });
    };
  }
})();