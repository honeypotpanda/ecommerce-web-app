(function() {
  angular
    .module('EcommerceApp')
    .controller('AdminDashboardCtrl', AdminDashboardCtrl);

  AdminDashboardCtrl.$inject = ['$mdDialog', 'Products', 'Toast'];

  function AdminDashboardCtrl($mdDialog, Products, Toast) {
    var vm = this;

    vm.product = {
      name: '',
      description: '',
      image: '',
      price: '',
      quantity: ''
    };

    vm.addProduct = function() {
      if (!vm.product.name) {
        vm.error = 'Name is required.';
        return;
      }

      vm.error = '';
      Products.create(vm.product)
        .success(function(res) {
          vm.products.push({
            name: res.name,
            description: res.description,
            image: res.image,
            price: res.price,
            quantity: res.quantity
          });

          vm.product = {
            name: '',
            description: '',
            image: '',
            price: '',
            quantity: ''
          };
        })
        .error(function(res) {
          vm.error = res.message;
        });
    };

    vm.showAddForm = function(ev) {
      $mdDialog.show({
        controller: 'AddDialogCtrl',
        controllerAs: 'vm',
        templateUrl: '/admin/addProduct/addProductDialog.view.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose: true
      })
      .then(function(newProduct) {
        Toast.showSimpleToast('Successfully added ' + newProduct.name + '.');
        vm.products.push(newProduct);
      });
    };

    vm.showEditForm = function(ev, product) {
      $mdDialog.show({
        controller: 'EditDialogCtrl',
        controllerAs: 'vm',
        templateUrl: '/admin/editProduct/editProductDialog.view.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose: true,
        locals: {
          product: product
        }
      })
      .then(function(updatedProduct) {
        Toast.showSimpleToast('Successfully updated ' + updatedProduct.name + '.');
        for (var i = 0; i < vm.products.length; i++) {
          if (vm.products[i]._id === updatedProduct._id) {
            vm.products[i] = updatedProduct;
            return;
          }
        }
      });
    };

    vm.showDeleteForm = function(ev, product) {
      var deleteForm = $mdDialog.confirm()
        .title('Are you sure you want to delete ' + product.name + '?')
        .targetEvent(ev)
        .ok('Yes')
        .cancel('No');

      $mdDialog.show(deleteForm).then(function() {
        Products.deleteProduct(product._id)
          .success(function(res) {
            Toast.showSimpleToast('Successfully deleted ' + product.name + '.');
            vm.products.splice(vm.products.indexOf(product), 1);
          });
      });
    };

    vm.showCreateForm = function() {
      var modalInstance = $uibModal.open({
        templateUrl: '/admin/createProductModal/createProductModal.view.html',
        controller: 'CreateProductModalCtrl',
        controllerAs: 'vm'
      });

      modalInstance.result
        .then(function(newProduct) {
          vm.products.push(newProduct);
        });
    };

    Products.list()
      .success(function(res) {
        vm.products = res;
      })
      .error(function(res) {
        vm.error = res;
      });
  }
})();
