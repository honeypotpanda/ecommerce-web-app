(function() {
  angular
    .module('EcommerceApp')
    .controller('RegisterCtrl', RegisterCtrl);

  RegisterCtrl.$inject = ['$location', 'Authentication'];

  function RegisterCtrl($location, Authentication) {
    var vm = this;

    vm.credentials = {
      email: '',
      password: '',
      passwordConfirmation: ''
    };

    vm.onSubmit = function(credentials) {
      vm.error = '';

      if (!credentials.email || !credentials.password || !credentials.passwordConfirmation) {
        vm.error = 'All fields are required, please try again.';
        return false;
      }

      if (credentials.password !== credentials.passwordConfirmation) {
        vm.error = 'Passwords do not match.';
        return false;
      }

      vm.register(credentials);
    };

    vm.register = function(credentials) {
      vm.error = '';
      Authentication
        .register(credentials)
        .success(function(res) {
          $location.path('/');
        })
        .error(function(res) {
          if (res.code === 11000) {
            vm.error = 'Email has been registered already.';
          } else {
            vm.error = res;
          }
        });
    };
  }
})();
