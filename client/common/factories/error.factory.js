(function() {
  angular
    .module('EcommerceApp')
    .factory('Error', Error);

  Error.$inject = [];

  function Error() {
    return {
      invalidToken: false
    };
  }
})();