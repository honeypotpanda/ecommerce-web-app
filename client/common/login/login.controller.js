(function() {
  angular
    .module('EcommerceApp')
    .controller('LoginCtrl', LoginCtrl);

  LoginCtrl.$inject = ['$location', 'Authentication'];

  function LoginCtrl($location, Authentication) {
    var vm = this;

    vm.credentials = {
      email: '',
      password: ''
    };

    vm.credentials.isAdmin = $location.path().indexOf('/admin') !== -1;

    vm.onSubmit = function(credentials) {
      vm.error = '';
      if (!credentials.email || !credentials.password) {
        vm.error = 'Email and password are required, please try again.';
        return false;
      } else {
        vm.login(credentials);
      }
    };

    vm.login = function(credentials) {
      Authentication
        .login(credentials)
        .success(function(res) {
          if (vm.credentials.isAdmin) {
            $location.path('/admin/dashboard');
          } else {
            $location.path('/dashboard');
          }
        })
        .error(function(res) {
          vm.error = res.message;
        });
    };
  }
})();
