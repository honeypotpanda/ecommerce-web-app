(function() {
  angular
    .module('EcommerceApp')
    .directive('navbar', navbar);

  function navbar() {
    return {
      restrict: 'EA',
      templateUrl: '/common/directives/navbar/navbar.tmpl.html',
      controller: 'NavbarCtrl as navvm'
    };
  }
})();