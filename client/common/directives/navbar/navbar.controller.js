(function() {
  angular
    .module('EcommerceApp')
    .controller('NavbarCtrl', NavbarCtrl);

  NavbarCtrl.$inject = ['$location', '$mdToast', '$route', 'Authentication'];

  function NavbarCtrl($location, $mdToast, $route, Authentication) {
    var navvm = this;

    navvm.currentUser = Authentication.currentUser();

    navvm.logout = function() {
      Authentication.logout();
      if ($location.url() === '/') {
        $route.reload();
      } else {
        $location.path('/');
      }
    };

    navvm.showSimpleToast = function(message) {
      $mdToast.show(
        $mdToast.simple()
          .textContent(message)
          .position('bottom right')
          .hideDelay(3000)
      );
    };
  }
})();
