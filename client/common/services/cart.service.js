(function() {
  angular
    .module('EcommerceApp')
    .service('Cart', Cart);

  Cart.$inject = ['$http'];

  function Cart($http) {
    var get = function(customerId) {
      return $http.get('/api/customers/' + customerId + '/cart');
    };

    var add = function(customerId, productId, quantity) {
      return $http.post('/api/customers/' + customerId + '/cart/' + productId, {quantity: quantity});
    };

    var update = function(customerId, productId, quantity) {
      return $http.put('/api/customers/' + customerId + '/cart/' + productId, {quantity: quantity});
    };

    var remove = function(customerId, productId) {
      return $http.delete('/api/customers/' + customerId + '/cart/' + productId);
    };

    return {
      get: get,
      add: add,
      update: update,
      remove: remove
    };
  }
})();