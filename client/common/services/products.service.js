(function() {
  angular
    .module('EcommerceApp')
    .service('Products', Products);

  Products.$inject = ['$http'];

  function Products($http) {
    var list = function() {
      return $http.get('/api/products');
    };

    var create = function(product) {
      return $http.post('/api/products', product);
    };

    var readProduct = function(productId) {
      return $http.get('/api/products/' + productId);
    };

    var updateProduct = function(product) {
      return $http.put('/api/products/' + product._id, product);
    };

    var deleteProduct = function(productId) {
      return $http.delete('/api/products/' + productId);
    };

    return {
      list: list,
      create: create,
      updateProduct: updateProduct,
      readProduct: readProduct,
      deleteProduct: deleteProduct
    };
  }
})();