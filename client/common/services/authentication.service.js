(function() {
  angular
    .module('EcommerceApp')
    .service('Authentication', Authentication);

  Authentication.$inject = ['$http', '$window'];

  function Authentication($http, $window) {
    var saveToken = function(token) {
      $window.localStorage['ecommerce-token'] = token;
    };

    var getToken = function() {
      return $window.localStorage['ecommerce-token'];
    };

    var isLoggedIn = function() {
      var token = getToken();

      if (token) {
        var payload = JSON.parse($window.atob(token.split('.')[1]));
        return payload.exp > Date.now() / 1000;
      } else {
        return false;
      }
    };

    var currentUser = function() {
      if (isLoggedIn()) {
        var token = getToken();
        var payload = JSON.parse($window.atob(token.split('.')[1]));

        return {
          _id: payload._id,
          role: payload.role
        };
      }
    };

    var login = function(credentials) {
      var loginRoute = credentials.isAdmin ? '/admin/login' : '/auth/login';

      return $http.post(loginRoute, credentials)
        .success(function(res) {
          saveToken(res.token);
          return res.token;
        })
        .error(function(res) {
          return res;
        });
    };

    var logout = function() {
      $window.localStorage.removeItem('ecommerce-token');
    };

    var register = function(credentials) {
      return $http.post('/auth/register', credentials)
        .success(function(res) {
          saveToken(res.token);
          return res.token;
        })
        .error(function(res) {
          return res;
        });
    };

    var validateToken = function() {
      return $http.post('/auth/token');
    };

    return {
      isLoggedIn: isLoggedIn,
      currentUser: currentUser,
      saveToken: saveToken,
      getToken: getToken,
      login: login,
      logout: logout,
      register: register,
      validateToken: validateToken
    };
  }
})();