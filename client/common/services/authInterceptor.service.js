(function() {
  angular
    .module('EcommerceApp')
    .service('AuthInterceptor', AuthInterceptor);

  AuthInterceptor.$inject = ['$window'];

  function AuthInterceptor($window) {
    var request = function(config) {
      var token = $window.localStorage['ecommerce-token'];

      if (token) {
        config.headers.authorization = 'Bearer ' + token;
      }

      return config;
    };

    return {
      request: request
    };
  }
})();