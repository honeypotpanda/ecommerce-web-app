(function() {
  angular
    .module('EcommerceApp')
    .service('Braintree', Braintree);

  Braintree.$inject = ['$http'];

  function Braintree($http) {
    var getNonce = function() {
      return $http.get('/braintree/token');
    };

    var checkout = function(body) {
      return $http.post('/braintree/checkout', body);
    };

    return {
      getNonce: getNonce,
      checkout: checkout
    }
  }
})();