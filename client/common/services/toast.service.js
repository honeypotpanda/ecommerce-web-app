(function() {
  angular
    .module('EcommerceApp')
    .service('Toast', Toast);

  Toast.$inject = ['$mdToast'];

  function Toast($mdToast) {
    var showSimpleToast = function(message) {
      $mdToast.show(
        $mdToast.simple()
          .textContent(message)
          .position('bottom right')
          .hideDelay(3000)
      );
    };

    return {
      showSimpleToast: showSimpleToast
    };
  }
})();