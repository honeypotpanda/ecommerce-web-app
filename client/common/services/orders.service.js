(function() {
  angular
    .module('EcommerceApp')
    .service('Orders', Orders);

  Orders.$inject = ['$http'];

  function Orders($http) {
    var getOrders = function(customerId) {
      return $http.get('/api/customers/' + customerId + '/orders');
    };

    var addOrder = function(customerId) {
      return $http.post('/api/customers/' + customerId + '/orders');
    };

    return {
      getOrders: getOrders,
      addOrder: addOrder
    };
  }
})();