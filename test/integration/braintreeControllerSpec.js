var app = require('../../server');
var request = require('supertest');
var should = require('should');

describe('Braintree Controller Integration Tests', function() {
  describe('GET /braintree/token', function() {
    it('should get a client token', function(done) {
      request(app)
        .get('/braintree/token')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200, done)
    });
  });

  describe('POST /braintree/checkout', function() {
    it('should complete transaction', function(done) {
      request(app)
        .post('/braintree/checkout')
        .set('Accept', 'application/json')
        .send({ paymentMethodNonce: 'fake-valid-visa-nonce' })
        .expect('Content-Type', /json/)
        .expect(200, done);
    });

    it('should not complete transaction', function(done) {
      var fromClient = {
        paymentMethodNonce: 'fake-processor-declined-visa-nonce',
        amount: 2500
      };

      request(app)
        .post('/braintree/checkout')
        .set('Accept', 'application/json')
        .send(fromClient)
        .expect('Content-Type', /json/)
        .expect(400, done);
    });
  })
});