module.exports = function(config) {
  config.set({
    basePath: '../',

    files: [
      'public/lib/angular/angular.js',
      'public/lib/angular-route/angular-route.js',
      'public/lib/angular-mocks/angular-mocks.js',
      'public/lib/angular-aria/angular-aria.js',
      'public/lib/angular-animate/angular-animate.js',
      'public/lib/angular-material/angular-material.js',
      'client/app.js',
      'client/**/*.js',
      'test/unit/client/**/*.js'
    ],

    autoWatch: true,

    frameworks: ['jasmine'],

    browsers: ['PhantomJS'],

    plugins: [
      'karma-jasmine',
      'karma-phantomjs-launcher',
      'karma-mocha-reporter'
    ],

    reporters: ['mocha']
  });
};