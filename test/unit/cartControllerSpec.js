var app = require('../../server');
var request = require('supertest');
var should = require('should');
var mongoose = require('mongoose');
var Admin = mongoose.model('Admin');
var Customer = mongoose.model('Customer');
var Product = mongoose.model('Product');

var adminAuth;
var authorizedCustomerAuth;
var unauthorizedCustomerAuth;
var invalidAuth;
var customer;
var product;

describe('Cart Controller Unit Tests', function() {
  before(function(done) {
    adminAuth = 'Bearer ' + Admin().generateJwt();
    unauthorizedCustomerAuth = 'Bearer ' + Customer().generateJwt();
    invalidAuth = 'Bearer invalidToken';
    done();
  });

  beforeEach(function(done) {
    product = new Product({
      name: 'toy',
      description: 'this is the best toy',
      image: 'image link',
      price: 1,
      quantity: 5
    });

    customer = new Customer({
      email: 'test@test.com',
      password: 123456,
      cart: [{
        productId: product._id,
        quantity: 1
      }]
    });

    authorizedCustomerAuth = 'Bearer ' + customer.generateJwt();

    customer.save(function() {
      product.save(function() {
        done();
      });
    });
  });

  describe('GET /api/customers/:customerId/cart', function() {
    it('should get cart with an authorized customer token', function(done) {
      request(app)
        .get('/api/customers/' + customer._id + '/cart')
        .set('Accept', 'application/json')
        .set('Authorization', authorizedCustomerAuth)
        .expect('Content-Type', /json/)
        .expect(200)
        .end(function(err, res) {
          if (err) {
            done(err);
          }

          res.body.should.be.an.Object();
          res.body.cart.should.be.an.Array().and.have.lengthOf(1);
          done();
        });
    });

    it('should not get cart with an unauthorized customer token', function(done) {
      request(app)
        .get('/api/customers/' + customer._id + '/cart/')
        .set('Accept', 'application/json')
        .set('Authorization', unauthorizedCustomerAuth)
        .expect('Content-Type', /json/)
        .expect(401, done);
    });

    it('should get cart with an admin token', function(done) {
      request(app)
        .get('/api/customers/' + customer._id + '/cart/')
        .set('Accept', 'application/json')
        .set('Authorization', adminAuth)
        .expect('Content-Type', /json/)
        .expect(401, done);
    });

    it('should get cart with an invalid token', function(done) {
      request(app)
        .get('/api/customers/' + customer._id + '/cart/')
        .set('Accept', 'application/json')
        .set('Authorization', invalidAuth)
        .expect('Content-Type', /json/)
        .expect(401, done);
    });
  });

  describe('POST /api/customers/:customerId/cart/:productId', function() {
    it("should add product to customer's cart with an authorized customer token", function(done) {
      request(app)
        .post('/api/customers/' + customer._id + '/cart/' + product._id)
        .set('Accept', 'application/json')
        .set('Authorization', authorizedCustomerAuth)
        .send({ quantity: 2 })
        .expect('Content-Type', /json/)
        .expect(200)
        .end(function(err, res) {
          if (err) {
            done(err);
          }

          res.body.should.be.an.Object();
          done();
        });
    });

    it("should not add product to customer's cart with an unauthorized customer token", function(done) {
      request(app)
        .post('/api/customers/' + customer._id + '/cart/' + product._id)
        .set('Accept', 'application/json')
        .set('Authorization', unauthorizedCustomerAuth)
        .send({ quantity: 2 })
        .expect('Content-Type', /json/)
        .expect(401, done);
    });

    it("should not add product to customer's cart with an admin token", function(done) {
      request(app)
        .post('/api/customers/' + customer._id + '/cart/' + product._id)
        .set('Accept', 'application/json')
        .set('Authorization', adminAuth)
        .send({ quantity: 2 })
        .expect('Content-Type', /json/)
        .expect(401, done);
    });

    it("should not add product to customer's cart with an invalid token", function(done) {
      request(app)
        .post('/api/customers/' + customer._id + '/cart/' + product._id)
        .set('Accept', 'application/json')
        .set('Authorization', invalidAuth)
        .send({ quantity: 2 })
        .expect('Content-Type', /json/)
        .expect(401, done);
    });

    it("should throw an error for requesting to add more product than what is available", function(done) {
      request(app)
        .post('/api/customers/' + customer._id + '/cart/' + product._id)
        .set('Accept', 'application/json')
        .set('Authorization', authorizedCustomerAuth)
        .send({ quantity: 6 })
        .expect('Content-Type', /json/)
        .expect(400, done)
    });
  });

  describe('PUT /api/customers/:customerId/cart/:productId', function() {
    it("should update item in customer's cart with an authorized customer token", function(done) {
      request(app)
        .put('/api/customers/' + customer._id + '/cart/' + product._id)
        .set('Accept', 'application/json')
        .set('Authorization', authorizedCustomerAuth)
        .send({ quantity: 2 })
        .expect('Content-Type', /json/)
        .expect(200)
        .end(function(err, res) {
          if (err) {
            done(err);
          }

          res.body.should.be.an.Object();
          done();
        });
    });

    it("should not update item in customer's cart with an unauthorized customer token", function(done) {
      request(app)
        .put('/api/customers/' + customer._id + '/cart/' + product._id)
        .set('Accept', 'application/json')
        .set('Authorization', unauthorizedCustomerAuth)
        .send({ quantity: 2 })
        .expect('Content-Type', /json/)
        .expect(401, done);
    });

    it("should not update item in customer's cart with an admin token", function(done) {
      request(app)
        .put('/api/customers/' + customer._id + '/cart/' + product._id)
        .set('Accept', 'application/json')
        .set('Authorization', adminAuth)
        .send({ quantity: 2 })
        .expect('Content-Type', /json/)
        .expect(401, done);
    });

    it("should not update item in customer's cart with an invalid token", function(done) {
      request(app)
        .put('/api/customers/' + customer._id + '/cart/' + product._id)
        .set('Accept', 'application/json')
        .set('Authorization', invalidAuth)
        .send({ quantity: 2 })
        .expect('Content-Type', /json/)
        .expect(401, done);
    });

    // it("should throw an error for requesting to add more product than what is available", function(done) {
    //   request(app)
    //     .post('/api/customers/' + customer._id + '/cart/' + product._id)
    //     .set('Accept', 'application/json')
    //     .set('Authorization', authorizedCustomerAuth)
    //     .send({ quantity: 6 })
    //     .expect('Content-Type', /json/)
    //     .expect(400, done)
    // });
  });

  describe('DELETE /api/customers/:customerId/cart/:productId', function() {
    it("should delete item in customer's cart with an authorized customer token", function(done) {
      request(app)
        .delete('/api/customers/' + customer._id + '/cart/' + product._id)
        .set('Accept', 'application/json')
        .set('Authorization', authorizedCustomerAuth)
        .expect('Content-Type', /json/)
        .expect(200, done);
    });

    it("should not delete item in customer's cart with an unauthorized customer token", function(done) {
      request(app)
        .delete('/api/customers/' + customer._id + '/cart/' + product._id)
        .set('Accept', 'application/json')
        .set('Authorization', unauthorizedCustomerAuth)
        .expect('Content-Type', /json/)
        .expect(401, done);
    });

    it("should not delete item in customer's cart with an admin token", function(done) {
      request(app)
        .delete('/api/customers/' + customer._id + '/cart/' + product._id)
        .set('Accept', 'application/json')
        .set('Authorization', adminAuth)
        .expect('Content-Type', /json/)
        .expect(401, done);
    });

    it("should not delete item in customer's cart with an invalid token", function(done) {
      request(app)
        .delete('/api/customers/' + customer._id + '/cart/' + product._id)
        .set('Accept', 'application/json')
        .set('Authorization', invalidAuth)
        .expect('Content-Type', /json/)
        .expect(401, done);
    });
  });

  afterEach(function(done) {
    Customer.remove().exec();
    Product.remove().exec();
    done();
  });
});