var app = require('../../server.js');
var should = require('should');
var mongoose = require('mongoose');
var Customer = mongoose.model('Customer');

var customer;

describe('Customer Model Unit Tests', function() {
  beforeEach(function(done) {
    customer = new Customer({
      email: 'test@test.com',
      password: 'a'.repeat(6)
    });

    done();
  });

  describe('Testing the save method', function() {
    it('should be able to save', function(done) {
      var password = customer.password;
      customer.save(function(err, newCustomer) {
        should.not.exist(err);
        should.exist(newCustomer);
        newCustomer.should.be.an.Object();
        should.notEqual(password, newCustomer.password);
        done();
      });
    });

    it('should lowercase email', function(done) {
      customer.email = 'TEST@TEST.COM';

      customer.save(function(err, newCustomer) {
        should.equal(newCustomer.email, 'test@test.com');
        done();
      });
    });

    it('should not be able to save a customer without an email', function(done) {
      customer.email = '';

      customer.save(function(err) {
        should.exist(err);
        done();
      });
    });
  });

  afterEach(function(done) {
    Customer.remove(function() {
      done();
    });
  });
});