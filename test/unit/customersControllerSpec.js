var app = require('../../server');
var request = require('supertest');
var should = require('should');
var mongoose = require('mongoose');
var Admin = mongoose.model('Admin');
var Customer = mongoose.model('Customer');

var adminAuth;
var authorizedCustomerAuth;
var unauthorizedCustomerAuth;
var invalidAuth;
var customer;
var newCustomer;

describe('Customers Controller Unit Tests', function() {
  before(function(done) {
    adminAuth = 'Bearer ' + Admin().generateJwt();
    unauthorizedCustomerAuth = 'Bearer ' + Customer().generateJwt();
    invalidAuth = 'Bearer invalidToken';
    newCustomer = {
      email: 'test2@test.com',
      password: 1234567
    };
    done();
  });

  beforeEach(function(done) {
    customer = new Customer({
      email: 'test@test.com',
      password: 123456
    });

    authorizedCustomerAuth = 'Bearer ' + customer.generateJwt();

    customer.save(function() {
      done();
    });
  });

  describe('GET /api/customers', function() {
    it('should get a list of customers with a valid admin token', function(done) {
      request(app)
        .get('/api/customers')
        .set('Accept', 'application/json')
        .set('Authorization', adminAuth)
        .expect('Content-Type', /json/)
        .expect(200)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }

          res.body.should.be.an.Array().and.have.lengthOf(1);
          res.body[0].should.have.property('email');
          done();
        });
    });

    it('should not get a list of customers with a valid customer token', function(done) {
      request(app)
        .get('/api/customers')
        .set('Accept', 'application/json')
        .set('Authorization', authorizedCustomerAuth)
        .expect('Content-Type', /json/)
        .expect(401, done);
    });

    it('should not get a list of customers with an invalid token', function(done) {
      request(app)
        .get('/api/customers')
        .set('Accept', 'application/json')
        .set('Authorization', invalidAuth)
        .expect('Content-Type', /json/)
        .expect(401, done);
    });
  });

  describe('GET /api/customers/:customerId', function() {
    it('should get an error', function(done) {
      request(app)
        .get('/api/customers/1')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(404, done);
    });

    it('should get a single customer', function(done) {
      request(app)
        .get('/api/customers/' + customer._id)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }

          res.body.should.be.an.Object().and.have.property('_id', customer.id);
          done();
        });
    });
  });

  describe('POST /api/customers', function() {
    it('should create a new customer with a valid admin token', function(done) {
      request(app)
        .post('/api/customers')
        .set('Accept', 'application/json')
        .set('Authorization', adminAuth)
        .send(newCustomer)
        .expect(200, done);
    });

    it('should not create a new customer with a valid customer token', function(done) {
      request(app)
        .post('/api/customers')
        .set('Accept', 'application/json')
        .set('Authorization', authorizedCustomerAuth)
        .send(newCustomer)
        .expect(401, done);
    });

    it('should not create a new customer with an invalid token', function(done) {
      request(app)
        .post('/api/customers')
        .set('Accept', 'application/json')
        .set('Authorization', invalidAuth)
        .send(newCustomer)
        .expect(401, done);
    });
  });

  describe('PUT /api/customers/:customerId', function() {
    it('should update a customer with an authorized customer token', function(done) {
      request(app)
        .put('/api/customers/' + customer._id)
        .set('Accept', 'application/json')
        .set('Authorization', authorizedCustomerAuth)
        .send(newCustomer)
        .expect(200)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }

          res.body.should.be.an.Object().and.have.property('email', 'test2@test.com');
          done();
        });
    });

    it('should not update a customer with an unauthorized customer token', function(done) {
      request(app)
        .put('/api/customers/' + customer._id)
        .set('Accept', 'application/json')
        .set('Authorization', unauthorizedCustomerAuth)
        .send(newCustomer)
        .expect('Content-Type', /json/)
        .expect(401, done);
    });

    it('should not update a customer with a valid admin token', function(done) {
      request(app)
        .put('/api/customers/' + customer._id)
        .set('Accept', 'application/json')
        .set('Authorization', adminAuth)
        .send(newCustomer)
        .expect('Content-Type', /json/)
        .expect(401, done);
    });

    it('should not update a customer with an invalid token', function(done) {
      request(app)
        .put('/api/customers/' + customer._id)
        .set('Accept', 'application/json')
        .set('Authorization', invalidAuth)
        .send(newCustomer)
        .expect('Content-Type', /json/)
        .expect(401, done);
    });
  });

  describe('DELETE /api/customers/:customerId', function() {
    it('should delete a customer with an authorized customer token', function(done) {
      request(app)
        .delete('/api/customers/' + customer._id)
        .set('Accept', 'application/json')
        .set('Authorization', authorizedCustomerAuth)
        .expect(204)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }

          res.body.should.be.an.Object().and.be.empty;
          done();
        });
    });

    it('should not delete a customer with an unauthorized customer token', function(done) {
      request(app)
        .delete('/api/customers/' + customer._id)
        .set('Accept', 'application/json')
        .set('Authorization', unauthorizedCustomerAuth)
        .expect(401, done);
    });

    it('should not delete a customer with an admin token', function(done) {
      request(app)
        .delete('/api/customers/' + customer._id)
        .set('Accept', 'application/json')
        .set('Authorization', adminAuth)
        .expect(401, done);
    });

    it('should not delete a customer with an invalid token', function(done) {
      request(app)
        .delete('/api/customers/' + customer._id)
        .set('Accept', 'application/json')
        .set('Authorization', invalidAuth)
        .expect(401, done);
    });
  });

  afterEach(function(done) {
    Customer.remove().exec();
    done();
  });
});