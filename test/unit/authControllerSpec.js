var app = require('../../server');
var request = require('supertest');
var should = require('should');
var mongoose = require('mongoose');
var Customer = mongoose.model('Customer');

var customer;

describe('Auth Controller Unit Tests', function() {
  beforeEach(function(done) {
    customer = new Customer({
      email: 'example@example.com',
      password: 'a'.repeat(6)
    });

    customer.save(function() {
      done();
    });
  });

  describe('POST /auth/register', function() {
    it('should create a new customer', function(done) {
      request(app)
        .post('/auth/register')
        .set('Accept', 'application/json')
        .send({ email: 'test@test.com', password: 'a'.repeat(6) })
        .expect('Content-Type', /json/)
        .expect(200, done)
    });
  });

  describe('POST /auth/login', function() {
    it('should successfully login', function(done) {
      request(app)
        .post('/auth/login')
        .set('Accept', 'application/json')
        .send({ email: 'example@example.com', password: 'a'.repeat(6) })
        .expect('Content-Type', /json/)
        .expect(200, done)
    });

    it('should not successfully login with an invalid email', function(done) {
      request(app)
        .post('/auth/login')
        .set('Accept', 'application/json')
        .send({ email: 'test@example.com', password: 'a'.repeat(6) })
        .expect('Content-Type', /json/)
        .expect(401, done)
    });

    it('should not successfully login with an invalid password', function(done) {
      request(app)
        .post('/auth/login')
        .set('Accept', 'application/json')
        .send({ email: 'example@example.com', password: 'a'.repeat(7) })
        .expect('Content-Type', /json/)
        .expect(401, done)
    });
  });

  afterEach(function(done) {
    Customer.remove().exec();
    done();
  });
});