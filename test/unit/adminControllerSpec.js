var app = require('../../server');
var request = require('supertest');
var should = require('should');
var mongoose = require('mongoose');
var Admin = mongoose.model('Admin');

var admin;

describe('Admin Controller Unit Tests', function() {
  beforeEach(function(done) {
    admin = new Admin({
      email: 'admin@admin.com',
      password: 'admin'
    });

    admin.save(function() {
      done();
    });
  });

  describe('POST /admin/login', function() {
    it('should successfully login', function(done) {
      request(app)
        .post('/admin/login')
        .set('Accept', 'application/json')
        .send({ email: 'admin@admin.com', password: 'admin' })
        .expect('Content-Type', /json/)
        .expect(200, done)
    });

    it('should not successfully login with an invalid email', function(done) {
      request(app)
        .post('/auth/login')
        .set('Accept', 'application/json')
        .send({ email: 'test@example.com', password: 'admin' })
        .expect('Content-Type', /json/)
        .expect(401, done)
    });

    it('should not successfully login with an invalid password', function(done) {
      request(app)
        .post('/auth/login')
        .set('Accept', 'application/json')
        .send({ email: 'admin@admin.com', password: 'admin' })
        .expect('Content-Type', /json/)
        .expect(401, done)
    });
  });

  afterEach(function(done) {
    Admin.remove().exec();
    done();
  });
});