var app = require('../../server');
var request = require('supertest');
var should = require('should');
var mongoose = require('mongoose');
var Admin = mongoose.model('Admin');
var Customer = mongoose.model('Customer');
var Product = mongoose.model('Product');

var adminAuth;
var authorizedCustomerAuth;
var unauthorizedCustomerAuth;
var invalidAuth;
var customer;
var product;

describe('Orders Controller Unit Tests', function() {
  before(function(done) {
    adminAuth = 'Bearer ' + Admin().generateJwt();
    unauthorizedCustomerAuth = 'Bearer ' + Customer().generateJwt();
    invalidAuth = 'Bearer invalidToken';
    done();
  });

  beforeEach(function(done) {
    customer = new Customer({
      email: 'test@test.com',
      password: 123456
    });

    authorizedCustomerAuth = 'Bearer ' + customer.generateJwt();

    product = new Product({
      name: 'toy',
      description: 'this is the best toy',
      image: 'image link',
      price: 1,
      quantity: 5
    });

    customer.cart.push({
      productId: product._id,
      quantity: 1
    });

    customer.save(function() {
      product.save(function() {
        done();
      });
    });
  });

  describe('POST /api/customers/:customerId/orders', function() {
    it('should add cart to orders with an authorized customer token', function(done) {
      request(app)
        .post('/api/customers/' + customer._id + '/orders')
        .set('Accept', 'application/json')
        .set('Authorization', authorizedCustomerAuth)
        .expect('Content-Type', /json/)
        .expect(200)
        .end(function(err, res) {
          if (err) {
            done(err);
          }

          res.body.should.be.an.Object();
          res.body.cart.should.be.an.Array().and.have.lengthOf(0);
          res.body.orders.should.be.an.Array().and.have.lengthOf(1);
          should.equal(res.body.orders[0].order[0].productId, product._id);
          should.equal(res.body.orders[0].order[0].quantity, 1);
          done();
        });
    });

    it('should not add cart to orders with an unauthorized customer token', function(done) {
      request(app)
        .post('/api/customers/' + customer._id + '/orders')
        .set('Accept', 'application/json')
        .set('Authorization', unauthorizedCustomerAuth)
        .expect('Content-Type', /json/)
        .expect(401, done);
    });

    it('should not add cart to orders with an admin token', function(done) {
      request(app)
        .post('/api/customers/' + customer._id + '/orders')
        .set('Accept', 'application/json')
        .set('Authorization', adminAuth)
        .expect('Content-Type', /json/)
        .expect(401, done);
    });

    it('should not add cart to orders with an invalid token', function(done) {
      request(app)
        .post('/api/customers/' + customer._id + '/orders')
        .set('Accept', 'application/json')
        .set('Authorization', invalidAuth)
        .expect('Content-Type', /json/)
        .expect(401, done);
    });

    it('should not add cart to orders', function(done) {
      customer.cart[0].quantity = 6;
      customer.save(function(err, customer) {
        if (err) {
          done(err);
        }

        request(app)
          .post('/api/customers/' + customer._id + '/orders')
          .set('Accept', 'application/json')
          .set('Authorization', authorizedCustomerAuth)
          .expect('Content-Type', /json/)
          .expect(400, done);
      });
    });
  });

  afterEach(function(done) {
    Customer.remove().exec();
    Product.remove().exec();
    done();
  });
});