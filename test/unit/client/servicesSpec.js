describe('Ecommerce App Services', function() {
  var $window;

  beforeEach(module('EcommerceApp'));
  beforeEach(inject(function(_$window_) {
    $window = _$window_;
  }));

  describe('Products', function() {
    it('should check the existence of Products service', inject(function(Products) {
      expect(Products).toBeDefined();
    }));
  });

  describe('Authentication', function() {
    var $httpBackend, Authentication, adminToken, adminCredentials;

    beforeEach(inject(function(_$httpBackend_, _Authentication_) {
      adminToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJfaWQiOiI1NzI1NDNmNjU1NGU1YTUxMDQ0YTYxY2UiLCJyb2xlIjoiYWRtaW4iLCJleHAiOjE0NjI2NjQ4MjIsImlhdCI6MTQ2MjA2MDAyMn0.7Q2tWJDspKLJMUJOnw_33d8BFzYpQE4C90QlX4P1RPg';

      $httpBackend = _$httpBackend_;
      $httpBackend.whenPOST('/admin/login')
        .respond({token: adminToken});
      Authentication = _Authentication_;
      adminCredentials = {
        isAdmin: true,
        email: 'admin@admin.com',
        password: 'admin'
      };
    }));

    afterEach(function() {
      Authentication.logout();
    });

    it('should check the existence of Authentication service', function() {
      expect(Authentication).toBeDefined();
    });

    it('should POST to /admin/login', function() {
      Authentication.login(adminCredentials);
      $httpBackend.flush();
      expect(Authentication.getToken()).toEqual(adminToken);
    });

    it('should check if token is present', function() {
      expect($window.localStorage['ecommerce-token']).toBeUndefined();
      Authentication.login(adminCredentials);
      $httpBackend.flush();
      expect($window.localStorage['ecommerce-token']).toBeDefined();
    });
  });

  describe('AuthInterceptor', function() {
    var AuthInterceptor;

    beforeEach(inject(function(_AuthInterceptor_) {
      AuthInterceptor = _AuthInterceptor_;
    }));

    it('should check the existence of AuthInterceptor service', function() {
      expect(AuthInterceptor).toBeDefined();
    });

    it('should set the authorization header', function() {
      var config = {headers: {}};
      expect(AuthInterceptor.request(config)).toEqual({headers: {}});
      $window.localStorage['ecommerce-token'] = 'token';
      expect(AuthInterceptor.request(config)).toEqual({
        headers: {
          authorization: 'Bearer token'
        }
      });
      $window.localStorage.removeItem('ecommerce-token');
    });
  });
});