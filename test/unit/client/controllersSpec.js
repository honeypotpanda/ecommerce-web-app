describe('Ecommerce App Controllers', function() {
  beforeEach(module('EcommerceApp'));

  describe('LoginCtrl', function() {
    var LoginCtrl, $location, $controller;

    beforeEach(inject(function(_$controller_, _$location_) {
      $controller = _$controller_;
      $location = _$location_;
      $location.path('/admin/login');
      LoginCtrl = $controller('LoginCtrl');
      spyOn(LoginCtrl, 'login');
    }));

    it('should initialize isAdmin to true', function() {
      expect(LoginCtrl.credentials).toEqual({isAdmin: true, email: '', password: ''});
    });

    it('should initialize isAdmin to false', function() {
      $location.path('/login');
      LoginCtrl = $controller('LoginCtrl');
      expect(LoginCtrl.credentials).toEqual({isAdmin: false, email: '', password: ''});
    });

    it('should update error model for email or password not being present', function() {
      expect(LoginCtrl.error).toBeUndefined();
      LoginCtrl.onSubmit({isAdmin: true, email: '', password: ''});
      expect(LoginCtrl.error).toBeTruthy();
      LoginCtrl.error = undefined;
      LoginCtrl.onSubmit({isAdmin: true, email: 'email', password: ''});
      expect(LoginCtrl.error).toBeTruthy();
      LoginCtrl.error = undefined;
      LoginCtrl.onSubmit({isAdmin: true, email: '', password: 'password'});
      expect(LoginCtrl.error).toBeTruthy();
    });

    it('should call login() for nonempty credentials', function() {
      var credentials = {isAdmin: true, email: 'email', password: 'password'};
      LoginCtrl.onSubmit(credentials);
      expect(LoginCtrl.login).toHaveBeenCalledWith(credentials);
    });
  });

  describe('RegisterCtrl', function() {
    var RegisterCtrl, $httpBackend, $location;

    beforeEach(inject(function($controller, _$httpBackend_, _$location_) {
      RegisterCtrl = $controller('RegisterCtrl');
      $httpBackend = _$httpBackend_;
      $httpBackend.whenPOST('/auth/register')
        .respond([200, {token: 'token'}]);
      $location = _$location_;
      $location.path('/register');
    }));

    it('should redirect to / after successful registration', function() {
      expect($location.path()).toEqual('/register');
      RegisterCtrl.register({email: 'test@test.com', password: '123456'});
      $httpBackend.flush();
      expect($location.path()).toEqual('/');
    });
  });

  describe('AdminDashboardCtrl', function() {
    var AdminDashboardCtrl, $httpBackend;

    beforeEach(inject(function($controller, _$httpBackend_) {
      AdminDashboardCtrl = $controller('AdminDashboardCtrl');
      $httpBackend = _$httpBackend_;
      $httpBackend.expectGET('/api/products')
        .respond([200, [{name: 'product1'}, {name: 'product2'}]]);
    }));

    it('should create products model and get two products from backend', function() {
      expect(AdminDashboardCtrl.products).toBeUndefined();
      $httpBackend.flush();
      expect(AdminDashboardCtrl.products).toBeDefined();
      expect(AdminDashboardCtrl.products.length).toEqual(2);
    });
  });
});