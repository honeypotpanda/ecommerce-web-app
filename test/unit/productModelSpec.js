var app = require('../../server.js');
var should = require('should');
var mongoose = require('mongoose');
var Product = mongoose.model('Product');

var product;

describe('Product Model Unit Tests', function() {
  beforeEach(function(done) {
    product = new Product({
      name: 'toy',
      description: 'this is the best toy',
      image: 'image link',
      price: 1,
      quantity: 5
    });

    done();
  });

  describe('Testing the save method', function() {
    it('should be able to save', function(done) {
      product.save(function(err, newProduct) {
        should.not.exist(err);
        should.exist(newProduct);
        newProduct.should.be.an.Object();
        done();
      });
    });

    it('should not be able to save a product without a name', function(done) {
      product.name = '';

      product.save(function(err) {
        should.exist(err);
        done();
      });
    });
  });

  afterEach(function(done) {
    Product.remove(function() {
      done();
    });
  });
});