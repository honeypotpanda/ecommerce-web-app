var app = require('../../server');
var request = require('supertest');
var should = require('should');
var mongoose = require('mongoose');
var Admin = mongoose.model('Admin');
var Customer = mongoose.model('Customer');
var Product = mongoose.model('Product');

var adminAuth;
var customerAuth;
var invalidAuth;
var product;
var newProduct;

describe('Products Controller Unit Tests', function() {
  before(function(done) {
    adminAuth = 'Bearer ' + Admin().generateJwt();
    customerAuth = 'Bearer ' + Customer().generateJwt();
    invalidAuth = 'Bearer invalidToken';
    newProduct = {
      name: 'newToy',
      description: 'description',
      image: 'image',
      price: 1
    };
    done();
  });

  beforeEach(function(done) {
    product = new Product({
      name: 'toy',
      description: 'this is the best toy',
      image: 'image link',
      price: 1,
      quantity: 5
    });
    product.save(function() {
      done();
    });
  });

  describe('GET /api/products', function() {
    it('should get a list of products', function(done) {
      request(app)
        .get('/api/products')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }

          res.body.should.be.an.Array().and.have.lengthOf(1);
          res.body[0].should.have.property('name');
          done();
        });
    });
  });

  describe('GET /api/products/:productId', function() {
    it('should get an error', function(done) {
      request(app)
        .get('/api/products/1')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(404, done);
    });

    it('should get a single product', function(done) {
      request(app)
        .get('/api/products/' + product._id)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }

          res.body.should.be.an.Object().and.have.property('_id', product.id);
          done();
        });
    });
  });

  describe('POST /api/products', function() {
    it('should create a new product with a valid admin token', function(done) {
      request(app)
        .post('/api/products')
        .set('Authorization', adminAuth)
        .send(newProduct)
        .expect(200, done);
    });

    it('should not create a new product with a valid customer token', function(done) {
      request(app)
        .post('/api/products')
        .set('Authorization', customerAuth)
        .send(newProduct)
        .expect(401, done);
    });

    it('should not create a new product with an invalid token', function(done) {
      request(app)
        .post('/api/products')
        .set('Authorization', invalidAuth)
        .send(newProduct)
        .expect(401, done);
    });
  });

  describe('PUT /api/products/:productId', function() {
    it('should update a product with a valid admin token', function(done) {
      request(app)
        .put('/api/products/' + product._id)
        .set('Accept', 'application/json')
        .set('Authorization', adminAuth)
        .send({ name: 'toy2' })
        .expect('Content-Type', /json/)
        .expect(200)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }

          res.body.should.be.an.Object().and.have.property('name', 'toy2');
          done();
        });
    });

    it('should not update a product with a valid customer token', function(done) {
      request(app)
        .put('/api/products/' + product._id)
        .set('Accept', 'application/json')
        .set('Authorization', customerAuth)
        .send({ name: 'toy2' })
        .expect('Content-Type', /json/)
        .expect(401, done);
    });

    it('should not update a product with an invalid token', function(done) {
      request(app)
        .put('/api/products/' + product._id)
        .set('Accept', 'application/json')
        .set('Authorization', invalidAuth)
        .send({ name: 'toy2' })
        .expect('Content-Type', /json/)
        .expect(401, done);
    });
  });

  describe('DELETE /api/products/:productId', function() {
    it('should delete a product with a valid admin token', function(done) {
      request(app)
        .delete('/api/products/' + product._id)
        .set('Accept', 'application/json')
        .set('Authorization', adminAuth)
        .expect(204)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }

          res.body.should.be.an.Object().and.be.empty;
          done();
        });
    });

    it('should not delete a product with a valid customer token', function(done) {
      request(app)
        .delete('/api/products/' + product._id)
        .set('Accept', 'application/json')
        .set('Authorization', customerAuth)
        .expect(401, done);
    });

    it('should not delete a product with an invalid token', function(done) {
      request(app)
        .delete('/api/products/' + product._id)
        .set('Accept', 'application/json')
        .set('Authorization', invalidAuth)
        .expect(401, done);
    });
  });

  afterEach(function(done) {
    Product.remove().exec();
    done();
  });
});