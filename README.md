Follow the steps in order to run the application:
(These steps assume that MongoDB, npm, and bower have been installed)
1. npm install
2. bower install
3. mongo
4. Create a .env file in the project root folder with the following line inside the file: JWT_SECRET=<YOUR SECRET KEY>
5. gulp
6. Visit localhost:4000 on your preferred browser