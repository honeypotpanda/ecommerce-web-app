var gulp = require('gulp');
var nodemon = require('gulp-nodemon');
var mocha = require('gulp-mocha');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var watch = require('gulp-watch');
var plumber = require('gulp-plumber');
var Server = require('karma').Server;
var cleanCSS = require('gulp-clean-css');

gulp.task('nodemon', function() {
  nodemon({
    script: 'server.js',
    env: { 'NODE_ENV': 'development' }
  });
});

gulp.task('test', function() {
  process.env.NODE_ENV = 'test';
  process.env.PORT = 7000;

  return gulp.src(['test/**/*.js', '!test/unit/client/**/*.js'], {read: false})
    .pipe(mocha({reporter: 'nyan'}));
});

function compress() {
  return gulp.src('client/**/*.js')
    .pipe(plumber())
    .pipe(concat('ecommerce-web-app.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('public/dist'));
}

function minifyCSS() {
  return gulp.src('public/css/**/*.css')
    .pipe(plumber())
    .pipe(concat('style.min.css'))
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('public/dist'));
}

gulp.task('compress', compress);

gulp.task('minify-css', minifyCSS);

gulp.task('watch', function() {
  watch('client/**/*.js', compress);
  watch('public/css/**/*.css', minifyCSS);
});

gulp.task('karma', function(done) {
  new Server({
    configFile: __dirname + '/test/karma.conf.js',
    singleRun: true
  }, done).start();
});

gulp.task('default', ['compress', 'minify-css', 'nodemon', 'watch']);